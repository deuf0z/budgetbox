package com.budgetbox;

import static io.restassured.RestAssured.given;

import java.util.List;
import java.util.Map;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import com.budgetbox.domain.Food;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
public class FoodTest {

    @Test
    @Order(1)
    public void testFoodEndpoint() {
        given().when().get("/food").then().statusCode(Status.OK.getStatusCode());
    }

    @Test
    @Order(2)
    public void testInsertFood() {
        final Food food = new Food();
        food.setFoodName("Saucisse");
        food.setScientificName("Saucissum");
        food.setFoodGroup("Viande");
        food.setFoodSubGroup("Barbecue");

        given().body(food).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON).when().post("/food").then()
                .statusCode(Status.CREATED.getStatusCode());
    }

    @Test
    @Order(3)
    public void testGetFoodByScientificName() {
        List<Map<String, String>> returnedFood = given().header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON).when()
                .get("/food/byScientificName/{scientificName}", "Saucissum").then().statusCode(200).extract().body()
                .jsonPath().getList(".");

        Assertions.assertTrue(!returnedFood.isEmpty());
        Assertions.assertTrue(returnedFood.size() == 1);
        Map<String, String> listFood = returnedFood.get(0);
        Assertions.assertTrue(listFood.get("foodName").equals("Saucisse"));
    }

    @Test
    @Order(4)
    public void testUpdateFood() {
        List<Map<String, Object>> returnedFood = given().header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON).when()
                .get("/food/byScientificName/{scientificName}", "Saucissum").then().statusCode(200).extract().body()
                .jsonPath().getList(".");
        
        Assertions.assertTrue(!returnedFood.isEmpty());
        Map<String, Object> listFood = returnedFood.get(0);
        Food food = new Food();
        food.setId(((Integer)listFood.get("id")).longValue());
        food.setFoodName("Knacki");
        food.setFoodGroup((String)listFood.get("foodGroup"));
        food.setFoodSubGroup((String)listFood.get("foodSubGroup"));
        food.setScientificName((String)listFood.get("scientificName"));

        given().body(food).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON).when().put("/food").then()
                .statusCode(Status.CREATED.getStatusCode());
    }

    @Test
    @Order(5)
    public void testGetFoodUpdatedByScientificName() {
        List<Map<String, String>> returnedFood = given().header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON).when()
                .get("/food/byScientificName/{scientificName}", "Saucissum").then().statusCode(200).extract().body()
                .jsonPath().getList(".");

        Assertions.assertTrue(!returnedFood.isEmpty());
        Assertions.assertTrue(returnedFood.size() == 1);
        Map<String, String> listFood = returnedFood.get(0);
        Assertions.assertTrue(listFood.get("foodName").equals("Knacki"));
    }

    @Test
    @Order(6)
    public void testDeleteFood() {
        List<Map<String, Object>> returnedFood = given().header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON).when()
                .get("/food/byScientificName/{scientificName}", "Saucissum").then().statusCode(200).extract().body()
                .jsonPath().getList(".");
        
        Assertions.assertTrue(!returnedFood.isEmpty());
        Map<String, Object> listFood = returnedFood.get(0);
        Food food = new Food();
        food.setId(((Integer)listFood.get("id")).longValue());
        food.setFoodName((String)listFood.get("foodName"));
        food.setFoodGroup((String)listFood.get("foodGroup"));
        food.setFoodSubGroup((String)listFood.get("foodSubGroup"));
        food.setScientificName((String)listFood.get("scientificName"));

        given().body(food).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON).when().delete("/food").then()
                .statusCode(Status.OK.getStatusCode());

        returnedFood = given().header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON).when()
                .get("/food/byScientificName/{scientificName}", "Saucissum").then().statusCode(200).extract().body()
                .jsonPath().getList(".");

        Assertions.assertTrue(returnedFood.isEmpty());
    }

}