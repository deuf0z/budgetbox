package com.budgetbox.service;

import java.util.List;
import java.util.function.Consumer;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.budgetbox.domain.Food;
import com.budgetbox.exception.FoodException;
import com.budgetbox.repository.FoodRepository;

@ApplicationScoped
public class DatabaseService {

    @Inject
    FoodRepository foodRepository;

    /** The logger. */
	private static final Logger LOG = Logger.getLogger(DatabaseService.class);

    /**
     * Insert dummy data in H2 database
     * @param foodList the list of Food objets to insert
     * @return boolean true if ok, else false
     */
    public boolean insertDummy(List<Food> foodList){
        boolean ok = true;
        Consumer<Food> consumerFood = f -> { try {
            foodRepository.insertFood(f);
        } catch (FoodException e) {
            LOG.error("Error in insertDummy() : "+e.getMessage());
            throw new RuntimeException();
        } };
        LOG.debug("Going to insert "+foodList.size()+" entries.");
        try{
            foodList.stream().forEach(consumerFood);
        } catch (RuntimeException e){
            ok = false;
        }
        return ok;
    }

}
