package com.budgetbox.api;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.budgetbox.domain.Food;
import com.budgetbox.exception.FoodException;
import com.budgetbox.repository.FoodRepository;

import org.jboss.logging.Logger;

@Path("/food")
@Produces("application/json")
@Consumes("application/json")
public class FoodResource {

    /** The logger. */
	private static final Logger LOG = Logger.getLogger(FoodResource.class);

    @Inject
    FoodRepository foodRepository;
    
    @GET
    public Response get() {
        List<Food> listFood = foodRepository.getAllFood();
        if (listFood != null)
            return Response.ok(listFood).status(Status.OK.getStatusCode()).build();
        else
            return Response.status(Status.NO_CONTENT.getStatusCode()).build();
    }

    @GET
    @Path("/byScientificName/{scientificName}")
    public Response getByScientificName(@PathParam(value="scientificName") String scientificName) {
        List<Food> listFood = foodRepository.getFoodByScientificName(scientificName);
        if (listFood != null)
            return Response.ok(listFood).status(Status.OK.getStatusCode()).build();
        else
            return Response.status(Status.NO_CONTENT.getStatusCode()).build();
    }

    @GET
    @Path("/byGroup/{group}")
    public Response getByGroup(@PathParam(value="group") String group) {
        List<Food> listFood = foodRepository.getFoodByGroup(group);
        if (listFood != null)
            return Response.ok(listFood).status(Status.OK.getStatusCode()).build();
        else
            return Response.status(Status.NO_CONTENT.getStatusCode()).build();
    }

    @GET
    @Path("/bySubGroup/{subGroup}")
    public Response getBySubGroup(@PathParam(value="subGroup") String subGroup) {
        List<Food> listFood = foodRepository.getFoodBySubGroup(subGroup);
        if (listFood != null)
            return Response.ok(listFood).status(Status.OK.getStatusCode()).build();
        else
            return Response.status(Status.NO_CONTENT.getStatusCode()).build();
    }

    @POST
    public Response create(Food food) {
        if (food != null){
            if(food.getFoodName() == null || food.getFoodName().isEmpty()){
                LOG.error("Error in create() : wrong food name");
                return Response.serverError().status(Status.BAD_REQUEST.getStatusCode()).build();
            }
            if(food.getScientificName() == null || food.getScientificName().isEmpty()){
                LOG.error("Error in create() : wrong scientific name");
                return Response.serverError().status(Status.BAD_REQUEST.getStatusCode()).build();
            }
            if(food.getFoodGroup() == null || food.getFoodGroup().isEmpty()){
                LOG.error("Error in create() : wrong group name");
                return Response.serverError().status(Status.BAD_REQUEST.getStatusCode()).build();
            }
            if(food.getFoodSubGroup() == null || food.getFoodSubGroup().isEmpty()){
                LOG.error("Error in create() : wrong subGroup name");
                return Response.serverError().status(Status.BAD_REQUEST.getStatusCode()).build();
            }
            try{
                foodRepository.insertFood(food);
                return Response.ok().status(Status.CREATED.getStatusCode()).build();
            } catch (FoodException e){
                LOG.error("Error in create() : "+e.getMessage());
                return Response.serverError().build();
            }   
        } else {
            LOG.error("Error in create() : food is null");
            return Response.serverError().status(Status.BAD_REQUEST.getStatusCode()).build();
        }
    }

    @PUT
    public Response update(Food food) {
        boolean ok = foodRepository.updateFood(food);
        if(ok)
            return Response.ok().status(Status.CREATED.getStatusCode()).build();
        else
            return Response.serverError().build();
    }

    @DELETE
    public Response delete(Food food) {
        boolean ok = foodRepository.remove(food);
        if(ok)
            return Response.ok().status(Status.OK.getStatusCode()).build();
        else
            return Response.serverError().build();
    }
}
