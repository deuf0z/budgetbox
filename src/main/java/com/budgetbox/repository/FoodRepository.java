package com.budgetbox.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.TransactionRequiredException;
import javax.transaction.Transactional;

import com.budgetbox.domain.Food;
import com.budgetbox.exception.FoodException;

import org.jboss.logging.Logger;

@ApplicationScoped
public class FoodRepository {

    @Inject
    EntityManager em;

    /** The logger. */
	private static final Logger LOG = Logger.getLogger(FoodRepository.class);

    @Transactional
    public List<Food> getAllFood(){
        List<Food> listFood = null;
        try {
            listFood = em.createQuery("SELECT f FROM Food f", Food.class)
                .getResultList();
        } catch (IllegalArgumentException e){
            LOG.error("Error in getAllFood() : "+e.getMessage());
        } 
        return listFood;
    }

    @Transactional
    public Food getFoodById(Long id){
        Food food = null;
        try {
            food = em.find(Food.class, id);
        } catch (IllegalArgumentException e){
            LOG.error("Error in getFoodById() : "+e.getMessage());
        } 
        return food;
    }

    @Transactional
    public List<Food> getFoodByName(String name){
        List<Food> listFood = null;
        try {
            listFood = em.createQuery(
                "SELECT f FROM Food f WHERE f.name LIKE :name", Food.class)
                .setParameter("name", name)
                .getResultList();
        } catch (IllegalArgumentException e){
            LOG.error("Error in getFoodByName() : "+e.getMessage());
        } 
        return listFood;
    }

    @Transactional
    public List<Food> getFoodByScientificName(String scientificName){
        List<Food> listFood = null;
        try {
            listFood = em.createQuery(
                "SELECT f FROM Food f WHERE f.scientificName LIKE :scientificName", Food.class)
                .setParameter("scientificName", scientificName)
                .getResultList();
        } catch (IllegalArgumentException e){
            LOG.error("Error in getFoodByScientificName() : "+e.getMessage());
        } 
        return listFood;
    }

    @Transactional
    public List<Food> getFoodByGroup(String group){
        List<Food> listFood = null;
        try {
            listFood = em.createQuery(
                "SELECT f FROM Food f WHERE f.foodGroup LIKE :foodGroup", Food.class)
                .setParameter("foodGroup", group)
                .getResultList();
        } catch (IllegalArgumentException e){
            LOG.error("Error in getFoodByGroup() : "+e.getMessage());
        } 
        return listFood;
    }

    @Transactional
    public List<Food> getFoodBySubGroup (String subGroup){
        List<Food> listFood = null;
        try {
            listFood = em.createQuery(
                "SELECT f FROM Food f WHERE f.foodSubGroup LIKE :foodSubGroup", Food.class)
                .setParameter("foodSubGroup", subGroup)
                .getResultList();
        } catch (IllegalArgumentException e){
            LOG.error("Error in getFoodBySubGroup() : "+e.getMessage());
        } 
        return listFood;
    }

    @Transactional
    public void insertFood(Food food) throws FoodException{
        if (food != null){
            try{
                em.persist(food);
            } catch(EntityExistsException|IllegalArgumentException|TransactionRequiredException e){
                LOG.error("Error in insertFood() : "+e.getMessage());
                throw new FoodException(e.getMessage());
            }
        } else {
            LOG.error("Error in insertFood() : food is null");
            throw new FoodException("Cannot insert null food.");
        }
    }

    @Transactional
    public boolean updateFood(Food food){
        boolean ok = true;
        try{
            if (food.getId() == null) {
                LOG.error("Error in updateFood() : food id is null");
                ok = false;
            }
            Food entity = em.find(Food.class, food.getId());
            if (entity == null) {
                LOG.error("Error in updateFood() : food not found with id "+food.getId());
                ok = false;
            }
            entity.setFoodName(food.getFoodName());
            entity.setScientificName(food.getScientificName());
            entity.setFoodGroup(food.getFoodGroup());
            entity.setFoodSubGroup(food.getFoodSubGroup());
        } catch(IllegalArgumentException e){
            LOG.error("Error in updateFood() : "+e.getMessage());
            ok = false;
        }
        return ok;
    }

    @Transactional
    public boolean remove(Food food){
        boolean ok = true;
        try{
            if (food.getId() == null) {
                LOG.error("Error in remove() : food id is null");
                ok = false;
            }
            Food entity = em.find(Food.class, food.getId());
            if (entity == null) {
                LOG.error("Error in remove() : food not found with id "+food.getId());
                ok = false;
            }
            em.remove(entity);
        } catch(IllegalArgumentException e){
            LOG.error("Error in remove() : "+e.getMessage());
            ok = false;
        }
        return ok;
    }
    
}
