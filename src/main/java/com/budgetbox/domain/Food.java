package com.budgetbox.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Food {

    @Id
    @SequenceGenerator(name = "foodSeq", sequenceName = "food_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(generator = "foodSeq")
    private Long id;
    private String foodName;
    private String scientificName;
    private String foodGroup;
    private String foodSubGroup;

    public Long getId(){
        return this.id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getFoodName(){
        return this.foodName;
    }

    public void setFoodName(String foodName){
        this.foodName = foodName;
    }

    public String getScientificName(){
        return this.scientificName;
    }

    public void setScientificName(String scientificName){
        this.scientificName = scientificName;
    }

    public String getFoodGroup(){
        return this.foodGroup;
    }

    public void setFoodGroup(String foodGroup){
        this.foodGroup = foodGroup;
    }

    public String getFoodSubGroup(){
        return this.foodSubGroup;
    }

    public void setFoodSubGroup(String foodSubGroup){
        this.foodSubGroup = foodSubGroup;
    }
    
}
