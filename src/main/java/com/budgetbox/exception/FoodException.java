package com.budgetbox.exception;

public class FoodException extends Exception {
    
    public FoodException(String errorMessage){
        super(errorMessage);
    }
}
