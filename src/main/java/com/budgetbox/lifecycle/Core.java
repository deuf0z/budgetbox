package com.budgetbox.lifecycle;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import com.budgetbox.domain.Food;
import com.budgetbox.service.DatabaseService;
import com.budgetbox.util.Util;

import org.jboss.logging.Logger;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;

@ApplicationScoped
public class Core {

    @Inject
    DatabaseService databaseService;
	
	/** The logger. */
	private static final Logger LOG = Logger.getLogger(Core.class);

    void onStart(@Observes StartupEvent ev) {
    	LOG.info("The application is starting.");

        LOG.info("Loading dummy data...");
        List<Food> listFood = Util.processInputFile();
        LOG.debug("listFood size : " + listFood.size());
        boolean inserted = databaseService.insertDummy(listFood);
        if (!inserted){
            LOG.error("Dummy data not inserted, application will stop.");
            System.exit(0);
        }
        LOG.info("Dummy data loaded.");
    }
    void onStop(@Observes ShutdownEvent ev) {
    	LOG.info("The application is stopping.");
    }
}
