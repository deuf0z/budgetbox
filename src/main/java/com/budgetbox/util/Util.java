package com.budgetbox.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.budgetbox.domain.Food;

import org.jboss.logging.Logger;

public class Util {
    private static final String SEPARATOR = ",";
    private static final String GENERIC_FOOD_CSV = "generic-food.csv";

    /** The logger. */
	private static final Logger LOG = Logger.getLogger(Util.class);
    
    /**
     * @param filePath the path of the generic-food file
     * @return List<Food> the list of Food objects filled with values in the file
     */
    public static List<Food> processInputFile() {
        List<Food> foodList = new ArrayList<Food>();
        try{
          ClassLoader classLoader = Util.class.getClassLoader();
          File file = new File(classLoader.getResource(GENERIC_FOOD_CSV).getFile());
          InputStream fileInputStream = new FileInputStream(file);
          BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream));
          foodList = br.lines().skip(1).map(mapToFood).collect(Collectors.toList());
          br.close();
        } catch (IOException e) {
          LOG.error("Error while reading the file : "+e.getMessage());
        }
        return foodList ;
    }

    /**
     * Transform a csv line into Food object
     */
    private static Function<String, Food> mapToFood = (line) -> {
        String[] s = line.split(SEPARATOR);
        Food food = new Food();
        if(s.length == 4){
            food.setFoodName(s[0]);
            food.setScientificName(s[1]);
            food.setFoodGroup(s[2]);
            food.setFoodSubGroup(s[3]);
        }

        return food;
    };
    
}
